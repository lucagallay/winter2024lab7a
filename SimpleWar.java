public class SimpleWar {
    public static void main (String[] args) {
        Deck deck = new Deck();
        int pointsP1 = 0;
        int pointsP2 = 0;

        while (deck.length() > 0) {
            deck.shuffle();
            Card cardP1 = deck.drawTopCard();
            Card cardP2 = deck.drawTopCard();

            System.out.println("Player 1's card: " + cardP1);
            System.out.println("Player 2's card: " + cardP2);

            System.out.print("Winning card: ");

            if (cardP1.calculateScore() > cardP2.calculateScore()) {
                System.out.println(cardP1);
                pointsP1 += 1;
            }
            else if (cardP1.calculateScore() < cardP2.calculateScore()) {
                System.out.println(cardP2);
                pointsP2 += 1;
            }
            
            System.out.println("Player 1's points: " + pointsP1);
            System.out.println("Player 2's points: " + pointsP2);
            System.out.println("----------------------------------------" + "\n");
        }

            if (pointsP1 > pointsP2) {
                System.out.println("Congratulations Player 1!");
            }
            else if (pointsP1 < pointsP2) {
                System.out.println("Congratulations Player 2!");
            }
            else {
                System.out.println("We have a tie!");
            }


    }



}
